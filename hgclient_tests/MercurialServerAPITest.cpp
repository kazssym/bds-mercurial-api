/*
 * MediaServerAPITest - DUnit tests for MediaServerAPI (implementation)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <System.hpp>
#pragma hdrstop

#include <MercurialServerAPI.h>
#include <TestFramework.hpp>

namespace {

    class TTestMercurialServerConnection : public TTestCase {
    __published:
        void __fastcall TestParseFileStatus();

    public:
        __fastcall virtual TTestMercurialServerConnection(String Name)
                : TTestCase(Name) {
        }

        void __fastcall SetUp() {
        }

        void __fastcall TearDown() {
        }
    };
}

void __fastcall TTestMercurialServerConnection::TestParseFileStatus() {
    TFileStatus status;
    status = TMercurialServerConnection::ParseFileStatus(_D("? "));
    CheckTrue(status == TFileStatus::Unknown, "TFileStatus::Unknown");
    status = TMercurialServerConnection::ParseFileStatus(_D("I "));
    CheckTrue(status == TFileStatus::Ignored, "TFileStatus::Ignored");
    status = TMercurialServerConnection::ParseFileStatus(_D("C "));
    CheckTrue(status == TFileStatus::Clean, "TFileStatus::Clean");
    status = TMercurialServerConnection::ParseFileStatus(_D("M "));
    CheckTrue(status == TFileStatus::Modified, "TFileStatus::Modified");
    status = TMercurialServerConnection::ParseFileStatus(_D("A "));
    CheckTrue(status == TFileStatus::Added, "TFileStatus::Added");
    status = TMercurialServerConnection::ParseFileStatus(_D("R "));
    CheckTrue(status == TFileStatus::Removed, "TFileStatus::Removed");
    status = TMercurialServerConnection::ParseFileStatus(_D("! "));
    CheckTrue(status == TFileStatus::Missing, "TFileStatus::Missing");
    status = TMercurialServerConnection::ParseFileStatus(_D("CC"));
    CheckTrue(status == TFileStatus::Unknown, "TFileStatus::Unknown");
}

static void Register() {
    Testframework::RegisterTest(TTestMercurialServerConnection::Suite());
}
#pragma startup Register 33

