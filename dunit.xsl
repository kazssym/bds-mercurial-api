<?xml version="1.0" encoding="UTF-8"?>
<!--
  dunit.xsl - XSL transformation for DUnit
  Copyright (C) 2014 Kaz Nishimura

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output encoding="UTF-8"/>
  <xsl:strip-space elements="TestRun TestSuite Test Statistics"/>
  <xsl:variable name="class">
    <xsl:value-of select="//TestSuite/TestSuite/@name"/>
    <xsl:for-each select="//TestSuite/TestSuite/TestSuite">
      <xsl:text>.</xsl:text>
      <xsl:value-of select="@name"/>
    </xsl:for-each>
  </xsl:variable>
  <xsl:variable name="hours">
    <xsl:value-of select="substring-before(//Statistics/Stat[@name='Runtime']/@result, ':')"/>
  </xsl:variable>
  <xsl:variable name="minutes">
    <xsl:value-of select="substring-before(substring-after(//Statistics/Stat[@name='Runtime']/@result, ':'), ':')"/>
  </xsl:variable>
  <xsl:variable name="seconds">
    <xsl:value-of select="substring-after(substring-after(//Statistics/Stat[@name='Runtime']/@result, ':'), ':')"/>
  </xsl:variable>
  <xsl:template match="TestRun">
    <xsl:element name="testsuites">
      <xsl:element name="testsuite">
        <xsl:attribute name="name">
          <xsl:value-of select="$class"/>
        </xsl:attribute>
        <xsl:attribute name="timestamp">
          <xsl:value-of select="translate(//Statistics/Stat[@name='Finished At']/@result, ' /', 'T-')"/>
        </xsl:attribute>
        <xsl:attribute name="hostname">
          <xsl:text>localhost</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="tests">
          <xsl:value-of select="//Statistics/Stat[@name='Tests']/@result"/>
        </xsl:attribute>
        <xsl:attribute name="failures">
          <xsl:value-of select="//Statistics/Stat[@name='Failures']/@result"/>
        </xsl:attribute>
        <xsl:attribute name="errors">
          <xsl:value-of select="//Statistics/Stat[@name='Errors']/@result"/>
        </xsl:attribute>
        <xsl:attribute name="time">
          <xsl:value-of select="(($hours * 60) + $minutes) * 60 + $seconds"/>
        </xsl:attribute>
        <xsl:apply-templates/>
      </xsl:element>
    </xsl:element>
  </xsl:template>
  <xsl:template match="Test">
    <xsl:element name="testcase">
      <xsl:attribute name="name">
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xsl:attribute name="classname">
        <xsl:value-of select="$class"/>
      </xsl:attribute>
      <xsl:attribute name="time">
        <xsl:text>0</xsl:text>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@result = 'ERROR'">
          <xsl:element name="error">
            <xsl:attribute name="message">
              <xsl:value-of select="Message"/>
            </xsl:attribute>
            <xsl:attribute name="type">
              <xsl:value-of select="FailureType"/>
            </xsl:attribute>
          </xsl:element>
        </xsl:when>
        <xsl:when test="@result = 'FAILS'">
          <xsl:element name="failure">
            <xsl:attribute name="message">
              <xsl:value-of select="Message"/>
            </xsl:attribute>
            <xsl:attribute name="type">
              <xsl:value-of select="FailureType"/>
            </xsl:attribute>
          </xsl:element>
        </xsl:when>
        <xsl:when test="@result != 'PASS'">
          <xsl:element name="skipped"/>
        </xsl:when>
      </xsl:choose>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>