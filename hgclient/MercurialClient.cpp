/*
 * MercurialClient - Mercurial client unit (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <System.hpp>
#pragma hdrstop

#include "MercurialClient.h"
#include "MercurialServerAPI.h"
#include <tchar.h>
#include <XMLDoc.hpp>
#include <IOUtils.hpp>
#include <vector>
#include <cassert>

#pragma package(smart_init)

using namespace Mercurialclient;
using namespace Mercurialserverapi;
using namespace std;

namespace {


    /*
     * Mercurial server connection using pipe streams.
     */
    class TPipeServerConnection : public TMercurialServerConnection {
        typedef TMercurialServerConnection inherited;

    public:
        static TPipeServerConnection *__fastcall GetConnection(
                const UnicodeString Directory = UnicodeString()) {
            HANDLE senderHandles[2] = {
                INVALID_HANDLE_VALUE,
                INVALID_HANDLE_VALUE
            };
            HANDLE receiverHandles[2] = {
                INVALID_HANDLE_VALUE,
                INVALID_HANDLE_VALUE
            };
            try {
                SECURITY_ATTRIBUTES pipeAttributes = {
                    sizeof (SECURITY_ATTRIBUTES),
                };
                pipeAttributes.bInheritHandle = true;
                if (!CreatePipe(&senderHandles[0], &senderHandles[1],
                        &pipeAttributes, 0)) {
                    RaiseLastOSError();
                }
                if (!CreatePipe(&receiverHandles[0], &receiverHandles[1],
                        &pipeAttributes, 0)) {
                    RaiseLastOSError();
                }

                // These calls are not critical.
                SetHandleInformation(senderHandles[1], HANDLE_FLAG_INHERIT,
                        0);
                SetHandleInformation(receiverHandles[0], HANDLE_FLAG_INHERIT,
                        0);

                STARTUPINFOW startup = {
                    sizeof (STARTUPINFOW),
                };
                startup.dwFlags |= STARTF_USESTDHANDLES;
                startup.hStdInput = senderHandles[0];
                startup.hStdOutput = receiverHandles[1];
                startup.hStdError = GetStdHandle(STD_ERROR_HANDLE);

                // This must be writable.
                wchar_t commandLine[] =
                        L"hg serve --encoding UTF-8 --cmdserver pipe";
                const wchar_t *currentDirectory = NULL;
                if (Directory.Length() != 0) {
                    currentDirectory = Directory.c_str();
                }

                PROCESS_INFORMATION child;
                if (!CreateProcessW(NULL, commandLine, NULL, NULL, true,
                        DETACHED_PROCESS, NULL, currentDirectory, &startup,
                        &child)) {
                    RaiseLastOSError();
                }
                // These values are not used.
                CloseHandle(child.hThread);
                CloseHandle(child.hProcess);
            } catch (...) {
                if (receiverHandles[1] != INVALID_HANDLE_VALUE) {
                    CloseHandle(receiverHandles[1]);
                }
                if (receiverHandles[0] != INVALID_HANDLE_VALUE) {
                    CloseHandle(receiverHandles[0]);
                }
                if (senderHandles[1] != INVALID_HANDLE_VALUE) {
                    CloseHandle(senderHandles[1]);
                }
                if (senderHandles[0] != INVALID_HANDLE_VALUE) {
                    CloseHandle(senderHandles[0]);
                }
                throw;
            }
            CloseHandle(receiverHandles[1]);
            CloseHandle(senderHandles[0]);

            TPipeServerConnection *connection = NULL;
            try {
                THandleStream *send = NULL;
                THandleStream *receive = NULL;
                try {
                    send = new THandleStream(THandle(senderHandles[1]));
                    receive = new THandleStream(THandle(receiverHandles[0]));
                    connection = new TPipeServerConnection(send, receive,
                            TEncoding::UTF8, Directory);
                } catch (...) {
                    delete receive;
                    delete send;
                    throw;
                }

                connection->ExpectHello();
            } catch (...) {
                delete connection;
                throw;
            }
            return connection;
        }

        virtual __fastcall ~TPipeServerConnection() {
            DeleteStream(ReceivingStream);
            DeleteStream(SendingStream);
        }

    protected:
        static void __fastcall DeleteStream(TStream *Stream) {
            FileClose(static_cast<THandleStream *>(Stream)->Handle);
            delete Stream;
        }

        __fastcall TPipeServerConnection(THandleStream *SendStream,
                THandleStream *ReceiveStream, TEncoding *Encoding,
                const UnicodeString CurrentDirectory)
                : inherited(SendStream, ReceiveStream, Encoding) {
        }
    };

    /*
     * Mercurial repository for Mercurial clients.
     */
    class TClientRepository : public TInterfacedObject,
            private IMercurialRepository {
        typedef TInterfacedObject inherited;

    public:
        __fastcall TClientRepository(const UnicodeString Path,
            TMercurialServerConnection *Connection)
        {
            assert(TPath::IsPathRooted(Path));
            assert(TDirectory::Exists(Path));
            FPath = Path;
            FConnection = Connection;
        }

        // 'IHgRepository' methods.

        UnicodeString __stdcall GetPath() {
            return FPath;
        }

        TRepositoryFiles __stdcall GetFiles() {
            UnicodeString params[] = {
                _D("status"),
                _D("--cwd=") + FPath,
                _D("--all"),
            };

            TRepositoryFiles files;
            TStream *output = NULL;
            TStream *error = NULL;
            TTextReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();
                if (FConnection->RunCommand(params, 2, output, error) != 0) {
                    FConnection->RaiseCommandError(error);
                }
                // Parses the output for files.
                output->Seek(0, 0);
                outputReader = new TStreamReader(output,
                        FConnection->Encoding, false, 1024);
                while (outputReader->Peek() >= 0) {
                    UnicodeString line = outputReader->ReadLine();
                    if (line.Length() >= 2 && line[2] == _D(' ')) {
                        files.Length = files.Length + 1;
                        files[files.High].Name =
                                line.SubString(3, line.Length() - 2);
                        files[files.High].Status =
                                TMercurialServerConnection::
                                ParseFileStatus(line);
                    }
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
            return files;
        }

        void __stdcall Commit(const UnicodeString Message,
            const UnicodeString *FileNames, const int FileNames_High,
            const TCommitOptions Options) {
            UnicodeString paramsInit[] = {
                _D("commit"),
                _D("--cwd=") + FPath,
                _D("--message=") + Message,
            };
            vector<UnicodeString> params(paramsInit, paramsInit + 3);
            for (int i = 0; i != FileNames_High + 1; ++i) {
                params.push_back(FileNames[i]);
            }

            TStream *output = NULL;
            TStream *error = NULL;
            TTextReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();
                if (FConnection->RunCommand(&params[0], params.size() - 1,
                        output, error) != 0) {
                    FConnection->RaiseCommandError(error);
                }
                // The following code may be unnecessary.
                output->Seek(0, 0);
                outputReader = new TStreamReader(output,
                        FConnection->Encoding, false, 1024);
                while (outputReader->Peek() >= 0) {
                    UnicodeString line = outputReader->ReadLine();
                    //ShowMessage(_D("hg commit: ") + line);
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
        }

        // 'IUnknown' methods.
        INTFOBJECT_IMPL_IUNKNOWN(inherited);

    private:
        UnicodeString FPath;
        TMercurialServerConnection *FConnection;
    };

    /*
     * Implements <code>IMercurial</code> for Mercurial clients.
     */
    class TClientService : public TInterfacedObject,
            private IMercurialService {
        typedef TInterfacedObject inherited;

    public:
        __fastcall TClientService() {
            try {
                FClient = TPipeServerConnection::GetConnection();
            } catch (EMercurialError &) {
                OutputDebugString(_T("TClientService: got an error while")
                        " creating a client for the current directory");
            }
        }

        virtual __fastcall ~TClientService() {
            delete FClient;
        }

        // 'IHgVersionControlService' methods.

        UnicodeString __stdcall GetRepositoryPath(const UnicodeString Name) {
            TMercurialServerConnection *client = GetClient(Name);
            if (!client) {
                return UnicodeString();
            }

            UnicodeString directoryPath = TPath::GetFullPath(Name);
            directoryPath = TPath::GetDirectoryName(directoryPath);
            assert(directoryPath.Length() != 0);

            UnicodeString repositoryPath;
            TStream *output = NULL;
            TStream *error = NULL;
            TStreamReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();

                UnicodeString params[] = {
                    _D("root"), _D("--cwd=") + directoryPath
                };
                if (client->RunCommand(params, 1, output, error) == 0) {
                    output->Seek(0, 0);
                    outputReader = new TStreamReader(output, client->Encoding,
                            false, 1024);
                    repositoryPath = outputReader->ReadLine();
                } else {
                    OutputDebugString(_T("Got a Mercurial command error")
                            " for file:");
                    OutputDebugStringW(Name.c_str());
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
            return repositoryPath;
        }

        _di_IMercurialRepository __stdcall GetRepository(
                const UnicodeString Name) {
            _di_IMercurialRepository repository;
            UnicodeString rootPath = GetRepositoryPath(Name);
            if (rootPath.Length() != 0) {
                // FHgClient must be valid here.
                (new TClientRepository(rootPath, FClient))->
                        GetInterface(repository);
                assert(repository);
            }
            return repository;
        }

        TFileStatus __stdcall GetFileStatus(const UnicodeString Name) {
            TMercurialServerConnection *client = GetClient(Name);
            if (!client) {
                return TFileStatus::Unknown;
            }

            TFileStatus status = TFileStatus::Unknown;
            TStream *output = NULL;
            TStream *error = NULL;
            TStreamReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();

                UnicodeString params[] = {
                    _D("status"), _D("--all"), Name
                };
                if (client->RunCommand(params, 2, output, error) == 0) {
                    output->Seek(0, 0);
                    outputReader = new TStreamReader(output, client->Encoding,
                            false, 1024);

                    UnicodeString line = outputReader->ReadLine();
                    status = TMercurialServerConnection::
                            ParseFileStatus(line);
                } else {
                    OutputDebugString(_T("Got a Mercurial command error")
                            " for file:");
                    OutputDebugStringW(Name.c_str());
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
            return status;
        }

        _di_IXMLNode __stdcall GetFileHistory(const UnicodeString FileName) {
            TMercurialServerConnection *client = GetClient(FileName);
            if (!client) {
                return NULL;
            }

            _di_IXMLNode history;
            TStream *output = NULL;
            TStream *error = NULL;
            TStreamReader *outputReader = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();

                UnicodeString params[] = {
                    _D("log"), _D("--style=xml"), FileName
                };
                if (client->RunCommand(params, 2, output, error) == 0) {
                    output->Seek(0, 0);
                    outputReader = new TStreamReader(output, client->Encoding,
                            false, 1024);

                    UnicodeString xml = outputReader->ReadToEnd();
                    _di_IXMLDocument document = LoadXMLData(xml);
                    history = document->DocumentElement;
                } else {
                    OutputDebugString(_T("Got a Mercurial command error")
                            " for file:");
                    OutputDebugString(FileName.c_str());
                }
            } __finally {
                delete outputReader;
                delete error;
                delete output;
            }
            return history;
        }

        TStream *__stdcall Open(const UnicodeString FileName,
                const UnicodeString Revision) {
            TMercurialServerConnection *client = GetClient(FileName);
            if (!client) {
                return NULL;
            }

            TStream *output = NULL;
            TStream *error = NULL;
            __try {
                output = new TMemoryStream();
                error = new TMemoryStream();

                const UnicodeString params[] = {
                    _D("cat"), _D("-r"), Revision, FileName
                };
                if (client->RunCommand(params, 3, output, error) == 0) {
                    output->Seek(0, 0);
                } else {
                    OutputDebugString(_T("Got a Mercurial command error")
                    " for file:");
                    OutputDebugStringW(FileName.c_str());
                    delete output;
                }
            } __finally {
                delete error;
            }
            return output;
        }

        // 'IUnknown' methods.
        INTFOBJECT_IMPL_IUNKNOWN(inherited);

    protected:
        TPipeServerConnection *__fastcall GetClient(const UnicodeString FileName) {
            if (!FClient) {
                UnicodeString directoryPath = TPath::GetFullPath(FileName);
                directoryPath = TPath::GetDirectoryName(directoryPath);
                try {
                    FClient = TPipeServerConnection::GetConnection(directoryPath);
                } catch (...) {
                    OutputDebugString(_T("Got an error in getting a client")
                            " for directory:");
                    OutputDebugStringW(directoryPath.c_str());
                }
            }
            return FClient;
        }

    private:
        TPipeServerConnection *FClient;
    };
}

/*
 * TClientMercurialContext implementation.
 */

__fastcall TMercurialClient::TMercurialClient(TComponent *Owner)
    : inherited(Owner) {
}

_di_IMercurialService __fastcall TMercurialClient::GetMercurial() {
    _di_IMercurialService hg;
    TClientService *instance = new TClientService();
    instance->GetInterface(hg);
    assert(hg);
    return hg;
}
