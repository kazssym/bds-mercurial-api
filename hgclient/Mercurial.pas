{
  Mercurial - basic Mercurial types
  Copyright (C) 2012-2014 Kaz Nishimura

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
  more details.

  You should have received a copy of the GNU General Public License along with
  this program.  If not, see <http://www.gnu.org/licenses/>.
}

unit Mercurial;

interface

uses
  Classes, SysUtils, XMLIntf;

{$SCOPEDENUMS ON}

type
  {
    Base exception for this package.
  }
  EMercurialError = class(Exception);

  {
    Mercurial command error.
  }
  EMercurialCommandError = class(EMercurialError);

  {
    Status of a file.
  }
  TFileStatus = (Unknown, Ignored, Clean, Modified, Added, Removed, Missing);

  {
    File in a repository.
  }
  TRepositoryFile = record
    Name: string;
    Status: TFileStatus;
  end;

  TRepositoryFiles = array of TRepositoryFile;

  TCommitOptions = record
    { Fields will be added later. }
  end;

  {
    Interface to a Mercurial repository.
  }
  IMercurialRepository = interface
    ['{4A988C79-DA2C-409A-B464-E4A51232DFDF}']

    { Returns the absolute path to this repository. }
    function GetPath: string; stdcall;

    { Returns a list of all files in this repository. }
    function GetFiles(): TRepositoryFiles; stdcall;

    { Make a commit to the local repository. }
    procedure Commit(const Message: string; const FileNames: array of string;
        const Options: TCommitOptions); stdcall;

    { Absolute path to this repository. }
    property Path: string read GetPath;
  end;

  {
    Interface to the Mercurial backend.
  }
  IMercurialService = interface
    ['{FE12D452-BDB7-4284-9DBE-F480BF0A8F9B}']

    {
      Returns the root path of the repository that contains a file.
    }
    function GetRepositoryPath(const FileName: string): string; stdcall;

    {
      Returns the repository that contains a file.
    }
    function GetRepository(const FileName: string)
        : IMercurialRepository; stdcall;

    {
      Returns the status of a file.
    }
    function GetFileStatus(const FileName: string): TFileStatus; stdcall;

    {
      Returns the history of a file.
    }
    function GetFileHistory(const FileName: string): IXMLNode; stdcall;

    {
      Returns the content of a revision of a file as a stream.
      The returned stream must be destroyed.
    }
    function Open(const FileName, Revision: string): TStream; stdcall;
  end;

implementation

end.
