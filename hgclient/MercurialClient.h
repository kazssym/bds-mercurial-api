/*
 * MercurialClient - Mercurial client unit (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MercurialClientH
#define MercurialClientH 1

#include <Mercurial.hpp>

namespace Mercurialclient {

    /*
     * Factory for Mercurial frontends that are clients of the Mercurial
     * command server.
     */
    class PACKAGE TMercurialClient : public TComponent {
        typedef TComponent inherited;

    public:
        __fastcall TMercurialClient(TComponent *Owner);

        _di_IMercurialService __fastcall GetMercurial();
    };
}
#ifndef DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE
using namespace Mercurialclient;
#endif

#endif
