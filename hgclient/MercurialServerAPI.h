/*
 * MercurialServerAPI - Mercurial command server API unit (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MercurialServerAPIH
#define MercurialServerAPIH 1

#include <Mercurial.hpp>
#include <SysUtils.hpp>

namespace Mercurialserverapi {

    using namespace System;
    using namespace Sysutils;

    /*
     * Mercurial server connection.
     */
    class PACKAGE TMercurialServerConnection : public TObject {
        typedef TObject inherited;

    public:
        static TFileStatus __fastcall ParseFileStatus(
                const UnicodeString StatusLine);

        /*
         * Constructs this object with send and recceive streams.
         * The default encoding will be used if Encoding is a null pointer.
         */
        __fastcall TMercurialServerConnection(TStream *SendingStream,
                TStream *ReceivingStream, TEncoding *Encoding = NULL);

        TEncoding *__fastcall GetEncoding();

        /*
         * Expects an initial hello response from the server.
         */
        void __fastcall ExpectHello();

        /*
         * Runs a command on the server.
         */
        int __fastcall RunCommand(const UnicodeString *Args,
                const int Args_High, TStream *OutputStream = NULL,
                TStream *ErrorStream = NULL);

        void __fastcall RaiseCommandError(TStream *ErrorStream);

        __property TStream *SendingStream = {read = FSendingStream};
        __property TStream *ReceivingStream = {read = FReceivingStream};
        __property TEncoding *Encoding = {read = GetEncoding};

    protected:
        struct TChannelHeader {
            Byte Channel;
            int Length;
        };

        void __fastcall SendData(const void *Data, int Count);

        TChannelHeader __fastcall ReceiveChannelHeader();

    private:
        TStream *FSendingStream;
        TStream *FReceivingStream;
        TEncoding *FEncoding;
    };
}
#ifndef DELPHIHEADER_NO_IMPLICIT_NAMESPACE_USE
using namespace Mercurialserverapi;
#endif

#endif
