/*
 * MercurialServerAPI - Mercurial command server API unit (interface)
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <System.hpp>
#pragma hdrstop

#include "MercurialServerAPI.h"
#include <RTLConsts.hpp>

/*
 * These macros have been renamed in later C++Builder versions.
 */
#ifndef Rtlconsts_SArgumentNil
#define Rtlconsts_SArgumentNil System_Rtlconsts_SArgumentNil
#endif
#ifndef Rtlconsts_SArgumentOutOfRange
#define Rtlconsts_SArgumentOutOfRange System_Rtlconsts_SArgumentOutOfRange
#endif

#pragma package(smart_init)

using namespace Mercurialserverapi;

/*
 * TMercurialServerConnection implementation:
 */

TFileStatus __fastcall TMercurialServerConnection::ParseFileStatus(
        const UnicodeString StatusLine) {
    TFileStatus status = TFileStatus::Unknown;
    if (StatusLine.Length() >= 2 && StatusLine[2] == _D(' ')) {
        switch (StatusLine[1]) {
        case _D('C'):
            status = TFileStatus::Clean;
            break;
        case _D('M'):
            status = TFileStatus::Modified;
            break;
        case _D('A'):
            status = TFileStatus::Added;
            break;
        case _D('R'):
            status = TFileStatus::Removed;
            break;
        case _D('!'): // Missing or deleted unexpectedly
            status = TFileStatus::Missing;
            break;
        case _D('I'):
            status = TFileStatus::Ignored;
            break;
        }
    }
    return status;
}

__fastcall TMercurialServerConnection::TMercurialServerConnection(
        TStream *SendingStream, TStream *ReceivingStream, TEncoding *Encoding)
        : FSendingStream(SendingStream), FReceivingStream(ReceivingStream),
        FEncoding(Encoding) {
    if (!FSendingStream || !FReceivingStream) {
        throw EArgumentNilException(Rtlconsts_SArgumentNil);
    }
}

TEncoding *__fastcall TMercurialServerConnection::GetEncoding() {
    if (!FEncoding) {
        return TEncoding::Default;
    }
    return FEncoding;
}

void __fastcall TMercurialServerConnection::ExpectHello() {
    // Handles an initial hello message on the output channel.
    TChannelHeader header = ReceiveChannelHeader();
    if (header.Channel != Byte('o')) {
        // TODO: Use more specific exception type.
        throw EMercurialError(_D("Protocol error"));
    }

    DynamicArray<Byte> hello;
    hello.Length = header.Length;
    FReceivingStream->ReadBuffer(&hello[0], hello.Length);
    // TODO: Parse the hello message.
}

int __fastcall TMercurialServerConnection::RunCommand(
        const UnicodeString *Args, const int Args_High, TStream *OutputStream,
        TStream *ErrorStream) {
    if (Args_High < 0) {
        throw EArgumentOutOfRangeException(Rtlconsts_SArgumentOutOfRange);
    }

    DynamicArray<Byte> data;
    TStringBuilder *dataBuilder = NULL;
    __try {
        dataBuilder = new TStringBuilder(Args[0]);
        for (int i = 1; i != Args_High + 1; ++i) {
            dataBuilder->Append(L'\0');
            dataBuilder->Append(Args[i]);
        }
        data = Encoding->GetBytes(dataBuilder->ToString());
    } __finally {
        delete dataBuilder;
    }
    FSendingStream->WriteBuffer("runcommand\n", 11);
    SendData(&data[0], data.Length);

    // Receives channels until the command finishes.
    for (;;) {
        TChannelHeader header = ReceiveChannelHeader();
        if (islower(header.Channel) && header.Length >= 0) {
            DynamicArray<Byte> bytes;
            bytes.Length = header.Length;
            try {
                FReceivingStream->ReadBuffer(&bytes[0], bytes.Length);
            } catch (EReadError &e) {
                throw EMercurialError(e.ToString());
            }

            switch (header.Channel) {
            case 'o':
                if (OutputStream) {
                    OutputStream->WriteBuffer(&bytes[0], bytes.Length);
                }
                break;
            case 'd':
            case 'e':
                if (ErrorStream) {
                    ErrorStream->WriteBuffer(&bytes[0], bytes.Length);
                }
                break;
            case 'r':
                if (bytes.Length == 4) {
                    uint32_t nResult =
                            *reinterpret_cast<uint32_t *>(&bytes[0]);
                    return ntohl(nResult);
                }
                throw EMercurialError(_D("Protocol error")); // TODO: L18N
            default:
                break;
            }
        } else {
            throw EMercurialError(_D("Unexpected channel")); // TODO: L18N
        }
    }
}

void __fastcall TMercurialServerConnection::RaiseCommandError(
        TStream *ErrorStream) {
    ErrorStream->Seek(0, 0);

    UnicodeString message;
    TStreamReader *errorReader = NULL;
    __try {
        errorReader = new TStreamReader(ErrorStream, Encoding, false, 1024);
        message = errorReader->ReadToEnd().TrimRight();
    } __finally {
        delete errorReader;
    }
    throw EMercurialCommandError(message);
}

void __fastcall TMercurialServerConnection::SendData(const void *Data, int Length) {
    uint32_t nLength = htonl(Length);
    FSendingStream->WriteBuffer(&nLength, 4);
    FSendingStream->WriteBuffer(Data, Length);
}

TMercurialServerConnection::TChannelHeader __fastcall
        TMercurialServerConnection::ReceiveChannelHeader() {
    TChannelHeader header;
    try {
        FReceivingStream->ReadBuffer(&header.Channel, 1);

        uint32_t nLength;
        FReceivingStream->ReadBuffer(&nLength, 4);
        // The value of nLength is in the network byte order (big-endian).
        header.Length = ntohl(nLength);
    } catch (EReadError &e) {
        throw EMercurialError(e.ToString()); // TODO: L18N
    }
    return header;
}
