/*
 * Package configuration for RAD Studio XE
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * Copying and distribution of this file, with or without modification, are
 * permitted in any medium without royalty provided the copyright notice and
 * this notice are preserved.  This file is offered as-is, without any warranty.
 */

#include "../generic/config.h"
