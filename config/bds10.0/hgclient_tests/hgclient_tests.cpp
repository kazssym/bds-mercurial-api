/*
 * hgclient_tests - unit test driver for HgBDS Mercurial Client
 * Copyright (C) 2012-2014 Kaz Nishimura
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <System.hpp>
#pragma hdrstop

#include <tchar.h>
#include <XMLTestRunner.hpp>
#include <memory>
#include <cstdlib>

using namespace std;

#ifdef USEPACKAGES
#pragma comment(lib, "hgclient.bpi")
#else
#pragma comment(lib, "hgclient")
#endif

int _tmain() {
    auto_ptr<TTestResult> result(Xmltestrunner::RunRegisteredTests());
    return result->WasSuccessful() ? EXIT_SUCCESS : EXIT_FAILURE;
}
