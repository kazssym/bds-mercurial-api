# READ ME

This directory contains the source code for a [Mercurial][] client package for
Embarcadero [RAD Studio][].

This package should provide a [Delphi][] and [C++Builder][] binding for
Mercurial commands.
It is a spin-off from the [HgBDS Mercurial Integration][] package to allow
other programs to use it.

[Mercurial]: <http://mercurial.selenic.com/>
[RAD Studio]: <http://www.embarcadero.com/products/rad-studio>
[Delphi]: <http://www.embarcadero.com/products/delphi>
[C++Builder]: <http://www.embarcadero.com/products/cbuilder>
[HgBDS Mercurial Integration]: <http://hgbds.vx68k.org/mercurial-integration>

## License

This program is *[free software][]*: you can redistribute it and/or modify it
under the terms of the [GNU General Public License][] as published by the
[Free Software Foundation][], either version 3 of the License, or (at your
option) any later version.

You should be able to receive a copy of the GNU General Public License along
with this program.

[free software]: <http://www.gnu.org/philosophy/free-sw.html> "What is free software?"
[GNU General Public License]: <http://www.gnu.org/licenses/gpl.html>
[Free Software Foundation]: <http://www.fsf.org/>
